if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register(new URL('./sw.js', __dirname), { scope: '/' })
  .then(r => {
    console.log('[SW]: Registered!', r);
  });
  navigator.serviceWorker.startMessages();
  console.log(navigator.serviceWorker.controller?.state);
} else {
  console.error('Service Worker is not available!');
}
