import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './modules/app/app.module';

ReactDOM.render(
  React.createElement(App, {}),
  document.getElementById('root')
);
