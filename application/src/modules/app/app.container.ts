import React from 'react';

import './app.style.scss';
import { App } from './app.view';

export function AppContainer(): React.ReactElement {
  return React.createElement(App, {});
}
