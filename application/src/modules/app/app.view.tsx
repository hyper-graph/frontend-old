import React from 'react';

export function App(): React.ReactElement {
  return (
    <React.StrictMode>
      <h1 className={'app-header'}>App</h1>
    </React.StrictMode>
  );
}
