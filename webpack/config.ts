import { resolve } from 'path';

import { Configuration, HotModuleReplacementPlugin } from 'webpack';
import { Configuration as DevServerConfiguration } from 'webpack-dev-server';

import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssPlugin, { loader as miniCssLoader } from 'mini-css-extract-plugin';

declare module 'webpack' {
  interface Configuration {
    devServer?: DevServerConfiguration;
  }
}

const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const dist = resolve(__dirname, '..', 'dist');

const tsLoader = {
  test: /\.tsx?$/,
  use: 'ts-loader',
  exclude: /node_modules/,
};

const webConfig: Configuration = {
  mode,
  stats: 'minimal',
  name: 'web',
  target: 'web',
  devtool: 'source-map',
  entry: '@hg/application',
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          miniCssLoader,
          'css-loader',
          'sass-loader',
        ],
      },
      tsLoader,
    ],
  },
  resolve: {
    extensions: [
      // Styles
      '.css',
      '.scss',
      // Images
      '.jpg',
      '.jpeg',
      '.png',
      '.svg',
      '.gif',
      '.webp',
      // Code
      '.tsx',
      '.ts',
      '.js',
      '.json',
    ],
  },
  plugins: [
    new MiniCssPlugin({
      filename: 'styles.css',
    }),
    new HtmlWebpackPlugin({
      hash: true,
      inject: 'head',
      filename: 'index.html',
      scriptLoading: 'defer',
      minify: 'auto',
      template: resolve(__dirname, 'public', 'index.html'),
    }),
    new HotModuleReplacementPlugin(),
  ],
  output: {
    filename: 'bundle.js',
    path: dist,
    publicPath: '/',
    assetModuleFilename: 'assets/images/[name][ext]',
  },
  devServer: {
    host: 'localhost',
    hot: true,
    historyApiFallback: true,
    index: 'index.html',
    port: 7000,
    disableHostCheck: true,
    publicPath: '/',
  },
};

const swConfig: Configuration = {
  mode,
  name: 'worker',
  stats: 'minimal',
  target: 'webworker',
  devtool: 'source-map',
  entry: '@hg/service-worker',
  module: {
    rules: [tsLoader],
  },
  resolve: {
    extensions: [
      '.ts',
      '.js',
    ],
  },
  output: {
    filename: 'sw.js',
    path: dist,
  },
};

const configs: [Configuration, Configuration] = [
  webConfig,
  swConfig,
];

export default configs;
